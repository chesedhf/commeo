//
//  ChatbotVC.swift
//  Commeo
//
//  Created by Chesedh Flauta on 11/02/2020.
//  Copyright © 2020 Chesedh Flauta. All rights reserved.
//

import UIKit
import ApiAI
import AVFoundation

class ChatbotVC: UIViewController {

    @IBOutlet weak var botLabel: UILabel!
    @IBOutlet weak var messageField: UITextField!
    
    let speechSynthesizer = AVSpeechSynthesizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func speechAndText(text: String) {
        let speechUtterance = AVSpeechUtterance(string: text)
        speechSynthesizer.speak(speechUtterance)    // read text
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseInOut, animations: {
            self.botLabel.text = text
        }, completion: nil) // animate text
    }

    @IBAction func send(_ sender: Any) {
        let request = ApiAI.shared().textRequest()
         
        if let text = self.messageField.text, text != "" {
            request?.query = text
        } else {
            return  // return if message is blank
        }
        
        request?.setMappedCompletionBlockSuccess({ (request, response) in
            let response = response as! AIResponse
            if let textResponse = response.result.fulfillment.messages {    // get response of Commeo from Dialogflow
                let resp = textResponse[0] as NSDictionary
                self.speechAndText(text: resp.value(forKey: "speech") as! String)
            }
        }, failure: { (request, error) in
            print(error!)
        })
        
        ApiAI.shared().enqueue(request) // send request to Dialogflow
        messageField.text = ""  // remove text in messageField
    }
    
}

