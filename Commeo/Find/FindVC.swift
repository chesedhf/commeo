//
//  FindVC.swift
//  Commeo
//
//  Created by Chesedh Flauta on 11/02/2020.
//  Copyright © 2020 Chesedh Flauta. All rights reserved.
//

import UIKit

class FindVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    // MARK: - UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2 // Header & Post Cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { return 1}
        else { return 5 }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 { return 120 }
        else { return 370 }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 { // Post Cell
            let postCell = tableView.dequeueReusableCell(withIdentifier: "FPostCell", for: indexPath) as! FPostCell
            
            return postCell
        }
        
        return tableView.dequeueReusableCell(withIdentifier: "FHeaderCell")!
    }

}
